package cn.dgut.service.impl;

import java.util.List;

import cn.dgut.pojo.User;




public interface IUserService {
	public void save(User user);
	public void delete(Integer id);
	public void update(User user);
	public List<User> getUserAll();
	public User getUserById(Integer id);
	
}
