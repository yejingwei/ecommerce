package cn.dgut.mapper;

import java.util.List;

import cn.dgut.pojo.User;

public interface UserMapper {
	public void add(User user);
	public void delete(Integer id);
	public void update(User user);
	public List<User> getUserAll();
	public User getUserById(Integer id);
	

}
